# uvm-extensions

Custom UVM extensions

## Extensions

### Packer

* it does one simple thing but very good. It packs and unpacks data without any
  additional bits
* very good for any kind of protocols (sequence items) like networking,
  encap/decap or streaming interfaces like AXI4-Stream or Avalon-ST
* new packer class inherited from the UVM packer class and extended The default
  UVM packer is replaced with UVM factory and `uvm_packer::set_default()` method
* no bits size limitation for packing data, unlimited
* using `foreach`, fixed slice of the array (`+:CONSTANT`), stream operators
  (`{>>{...}}`) and forward ahead memory allocation for the best performance
* reducing number of required allocation with the forward ahead memory
  allocation technique
* removed unnecessary magic 8 bytes with magic values at the beginning of a
  packed bit stream that stores internal packer state
* removed unnecessary magic bits during packing UVM objects
* proper packer initialization in constructor. Default UVM packer was not
  initialized correctly (`m_*_iter` was not set to `64` value but to `0`)
* code simplicity

### Coreservice

* new coreservice class inherited from the default UVM coreservice class and
  extended. The default UVM coreservice is replaced with
  `uvm_coreservice_t::set()` method
* adding the `.clone()` call for `.get_default_*()` methods in coreservice
* default packer object in packing and unpacking methods are now thread safe
* default packer object in packing and unpacking methods don't have any side
  effects (not shared state as a singleton)
