/* Copyright 2020 Tymoteusz Blazejczyk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

`ifndef UVM_EXTENSIONS_PACKER_SVH
`define UVM_EXTENSIONS_PACKER_SVH

/* Class: packer
 *
 * UVM packer extension
 *
 * Reason:
 * https://forums.accellera.org/topic/6572-annoying-and-useless-packer-implementation-in-the-uvm-2017-v10-library-code-release
 *
 * Default packer implementation have many drawbacks that this class fix it:
 * - limited size to 4088 bytes for user data
 * - unnecessary magic 8 bytes with magic values at the beginning of a packed bit stream that stores internal packer state
 * - unnecessary magic bits during packing UVM objects
 * - default UVM packer is not initialized correctly. It needs a direct call of the .flush() method
 * - unnecessary overcomplicated code complexity. Hard to read and debug
 */
class packer extends uvm_packer;
    `uvm_object_utils(uvm_extensions_pkg::packer)

    localparam DEFAULT_MEMORY_SIZE = 8 * `UVM_PACKER_MAX_BYTES;
    localparam FORWARD_ALLOCATION_SIZE = $bits(uvm_integral_t);

    /* Public */
    extern function new(string name = "");

    extern virtual function int get_packed_size();

    extern virtual function void set_packed_bits(ref bit unsigned stream[]);

    extern virtual function void set_packed_bytes(ref byte unsigned stream[]);

    extern virtual function void set_packed_ints(ref int unsigned stream[]);

    extern virtual function void set_packed_longints(ref longint unsigned stream[]);

    extern virtual function void get_packed_bits(ref bit unsigned stream[]);

    extern virtual function void get_packed_bytes(ref byte unsigned stream[]);

    extern virtual function void get_packed_ints(ref int unsigned stream[]);

    extern virtual function void get_packed_longints(ref longint unsigned stream[]);

    extern virtual function void flush();

    extern virtual function bit is_null();

    extern virtual function void pack_field(uvm_bitstream_t value, int size);

    extern virtual function void pack_field_int(uvm_integral_t value, int size);

    extern virtual function void pack_bits(ref bit value[], input int size = -1);

    extern virtual function void pack_bytes(ref byte value[], input int size = -1);

    extern virtual function void pack_ints(ref int value[], input int size = -1);

    extern virtual function void pack_object(uvm_object value);

    extern virtual function void pack_string(string value);

    extern virtual function void pack_time(time value);

    extern virtual function void pack_real(real value);

    extern virtual function uvm_bitstream_t unpack_field(int size);

    extern virtual function uvm_integral_t unpack_field_int(int size);

    extern virtual function void unpack_bits(ref bit value[], input int size = -1);

    extern virtual function void unpack_bytes(ref byte value[], input int size = -1);

    extern virtual function void unpack_ints(ref int value[], input int size = -1);

    extern virtual function string unpack_string();

    extern virtual function time unpack_time();

    extern virtual function real unpack_real();

    extern virtual function void unpack_object(uvm_object value);

    /* Private */
    extern local function int get_unpack_size(int size = -1);

    extern local function void allocate_memory(int size);

    extern local function void reallocate_memory(int size);

    extern local static function int align_bits_to_bytes(int bits);

    extern local static function int align_bits_to_ints(int bits);

    extern local static function int align_bits_to_longints(int bits);

    extern local static function byte mask_byte(byte value, int size);

    extern local static function int mask_int(int value, int size);

    extern local static function longint mask_longint(longint value, int size);

    local int m_packed = 0;
    local int m_unpacked = 0;
    local bit unsigned m_bits[];
endclass

function packer::new(string name = "");
    super.new(name);

    m_packed = 0;
    m_unpacked = 0;
    m_bits = new [DEFAULT_MEMORY_SIZE];
endfunction

function int packer::get_packed_size();
    return m_packed - m_unpacked;
endfunction

function bit packer::is_null();
    `uvm_warning("UVM/BASE/PACKER/NULL", ".is_null() method is not supported")

    return 1'b0;
endfunction

function void packer::flush();
    m_packed = 0;
    m_unpacked = 0;
endfunction

function void packer::set_packed_bits(ref bit unsigned stream[]);
    const int size = stream.size();

    if ((size + FORWARD_ALLOCATION_SIZE) > m_bits.size()) begin
        m_bits = new [size + FORWARD_ALLOCATION_SIZE + DEFAULT_MEMORY_SIZE] (stream);
    end
    else begin
        m_bits = new [m_bits.size()] (stream);
    end

    m_packed = size;
endfunction

function void packer::set_packed_bytes(ref byte unsigned stream[]);
    const int size = $bits(byte) * stream.size();
    int bits_index = 0;

    allocate_memory(size);

    foreach (stream[i]) begin
        m_bits[bits_index+:$bits(byte)] = {>>{stream[i]}};
        bits_index += $bits(byte);
    end

    m_packed = size;
endfunction

function void packer::set_packed_ints(ref int unsigned stream[]);
    const int size = $bits(int) * stream.size();
    int bits_index = 0;

    allocate_memory(size);

    foreach (stream[i]) begin
        m_bits[bits_index+:$bits(int)] = {>>{stream[i]}};
        bits_index += $bits(int);
    end

    m_packed = size;
endfunction

function void packer::set_packed_longints(ref longint unsigned stream[]);
    const int size = $bits(longint) * stream.size();
    int bits_index = 0;

    allocate_memory(size);

    foreach (stream[i]) begin
        m_bits[bits_index+:$bits(longint)] = {>>{stream[i]}};
        bits_index += $bits(longint);
    end

    m_packed = size;
endfunction

function void packer::get_packed_bits(ref bit unsigned stream[]);
    stream = new [m_packed] (m_bits);
endfunction

function void packer::get_packed_bytes(ref byte unsigned stream[]);
    int bits_index = 0;

    stream = new [align_bits_to_bytes(m_packed)];

    foreach (stream[i]) begin
        stream[i] = {>>{m_bits[bits_index+:$bits(byte)]}};
        bits_index += $bits(byte);
    end

    if (stream.size() > 0) begin
        const int last_index = stream.size() - 1;
        stream[last_index] = mask_byte(stream[last_index], m_packed);
    end
endfunction

function void packer::get_packed_ints(ref int unsigned stream[]);
    int bits_index = 0;

    stream = new [align_bits_to_ints(m_packed)];

    foreach (stream[i]) begin
        stream[i] = {>>{m_bits[bits_index+:$bits(int)]}};
        bits_index += $bits(int);
    end

    if (stream.size() > 0) begin
        const int last_index = stream.size() - 1;
        stream[last_index] = mask_int(stream[last_index], m_packed);
    end
endfunction

function void packer::get_packed_longints(ref longint unsigned stream[]);
    int bits_index = 0;

    stream = new [align_bits_to_longints(m_packed)];

    foreach (stream[i]) begin
        stream[i] = {>>{m_bits[bits_index+:$bits(longint)]}};
        bits_index += $bits(longint);
    end

    if (stream.size() > 0) begin
        const int last_index = stream.size() - 1;
        stream[last_index] = mask_longint(stream[last_index], m_packed);
    end
endfunction

function void packer::pack_field(uvm_bitstream_t value, int size);
    const int maximum_size = $bits(uvm_bitstream_t);

    if (size < 0) begin
        size = maximum_size;
    end
    else if (size > maximum_size) begin
        size = maximum_size;

        `uvm_error("UVM/BASE/PACKER/BAD_SIZE",
            $sformatf(".pack_field() method called with size '%0d', which exceeds maximum allowed size of '%0d'",
                size,
                maximum_size
            )
        )
    end

    reallocate_memory(size);

    for (int i = 0, value_index = (size - 1); i < size; ++i) begin
        m_bits[m_packed++] = value[value_index--];
    end
endfunction

function void packer::pack_field_int(uvm_integral_t value, int size);
    const int maximum_size = $bits(uvm_integral_t);

    if (size < 0) begin
        size = maximum_size;
    end
    else if (size > maximum_size) begin
        size = maximum_size;

        `uvm_error("UVM/BASE/PACKER/BAD_SIZE",
            $sformatf(".pack_field_int() method called with size '%0d', which exceeds maximum allowed size of '%0d'",
                size,
                maximum_size
            )
        )
    end

    reallocate_memory(size);

    /* Move all valid bits to the left, unused bits are on the right side */
    value <<= ($bits(uvm_integral_t) - size);

    m_bits[m_packed+:$bits(uvm_integral_t)] = {>>{value}};
    m_packed += size;
endfunction

function void packer::pack_bits(ref bit value[], input int size = -1);
    const int maximum_size = value.size();

    if (size < 0) begin
        size = maximum_size;
    end
    else if (size > maximum_size) begin
        size = maximum_size;

        `uvm_error("UVM/BASE/PACKER/BAD_SIZE",
            $sformatf(".pack_bits() method called with size '%0d', which exceeds maximum allowed size of '%0d'",
                size,
                maximum_size
            )
        )
    end

    reallocate_memory(size);

    for (int i = 0; i < size; ++i) begin
        m_bits[m_packed++] = value[i];
    end
endfunction

function void packer::pack_bytes(ref byte value[], input int size = -1);
    const int maximum_size = $bits(byte) * value.size();
    int bits_index = m_packed;
    int value_size = 0;

    if (size < 0) begin
        size = maximum_size;
    end
    else if (size > maximum_size) begin
        size = maximum_size;

        `uvm_error("UVM/BASE/PACKER/BAD_SIZE",
            $sformatf(".pack_bytes() method called with size '%0d', which exceeds maximum allowed size of '%0d'",
                size,
                maximum_size
            )
        )
    end

    reallocate_memory(size);

    value_size = align_bits_to_bytes(size);

    for (int i = 0; i < value_size; ++i) begin
        m_bits[bits_index+:$bits(byte)] = {>>{value[i]}};
        bits_index += $bits(byte);
    end

    m_packed += size;
endfunction

function void packer::pack_ints(ref int value[], input int size = -1);
    const int maximum_size = $bits(int) * value.size();
    int bits_index = m_packed;
    int value_size = 0;

    if (size < 0) begin
        size = maximum_size;
    end
    else if (size > maximum_size) begin
        size = maximum_size;

        `uvm_error("UVM/BASE/PACKER/BAD_SIZE",
            $sformatf(".pack_ints() method called with size '%0d', which exceeds maximum allowed size of '%0d'",
                size,
                maximum_size
            )
        )
    end

    reallocate_memory(size);

    value_size = align_bits_to_ints(size);

    for (int i = 0; i < value_size; ++i) begin
        m_bits[bits_index+:$bits(int)] = {>>{value[i]}};
        bits_index += $bits(int);
    end

    m_packed += size;
endfunction

function void packer::pack_string(string value);
    const int size = $bits(byte) * (value.len() + 1);
    int bits_index = m_packed;

    reallocate_memory(size);

    foreach (value[i]) begin
        m_bits[bits_index+:$bits(byte)] = {>>{value[i]}};
        bits_index += $bits(byte);
    end

    m_bits[bits_index+:$bits(byte)] = {>>{8'd0}};

    m_packed += size;
endfunction

function void packer::pack_time(time value);
    pack_field_int(value, $bits(time));
endfunction

function void packer::pack_real(real value);
    pack_field_int($realtobits(value), 64);
endfunction

function void packer::pack_object(uvm_object value);
    if (value != null) begin
        uvm_field_op field_op;

        push_active_object(value);

        field_op = uvm_field_op::m_get_available_op();
        field_op.set(UVM_PACK, this, value);
        value.do_execute_op(field_op);

        if (field_op.user_hook_enabled()) begin
            value.do_pack(this);
        end

        field_op.m_recycle();

        void'(pop_active_object());
    end
    else begin
        `uvm_error("UVM/BASE/PACKER/NULL", ".pack_object() method called with an UVM object as a null")
    end
endfunction

function uvm_bitstream_t packer::unpack_field(int size);
    uvm_bitstream_t value = '0;

    size = get_unpack_size(size);

    if (size > $bits(uvm_bitstream_t)) begin
        size = $bits(uvm_bitstream_t);

        `uvm_error("UVM/BASE/PACKER/BAD_SIZE",
            $sformatf(".unpack_field() method called with size '%0d', which exceeds maximum allowed size of '%0d'",
                size,
                $bits(uvm_bitstream_t)
            )
        )
    end

    for (int i = 0; i < size; ++i) begin
        value[i] = m_bits[m_unpacked++];
    end

    return value;
endfunction

function uvm_integral_t packer::unpack_field_int(int size);
    uvm_integral_t value = '0;

    size = get_unpack_size(size);

    if (size > $bits(uvm_integral_t)) begin
        size = $bits(uvm_integral_t);

        `uvm_error("UVM/BASE/PACKER/BAD_SIZE",
            $sformatf(".unpack_field_int() method called with size '%0d', which exceeds maximum allowed size of '%0d'",
                size,
                $bits(uvm_integral_t)
            )
        )
    end

    value = {>>{m_bits[m_unpacked+:$bits(uvm_integral_t)]}};
    m_unpacked += size;

    /* Move all valid bits to the right, unused bits are on the right side */
    value >>= ($bits(uvm_integral_t) - size);

    return value;
endfunction

function void packer::unpack_bits(ref bit value[], input int size = -1);
    size = get_unpack_size(size);
    value = new [size];

    for (int i = 0; i < size; ++i) begin
        value[i] = m_bits[m_unpacked++];
    end
endfunction

function void packer::unpack_bytes(ref byte value[], input int size = -1);
    int bits_index = m_unpacked;

    size = get_unpack_size(size);
    value = new [align_bits_to_bytes(size)];

    foreach (value[i]) begin
        value[i] = {>>{m_bits[bits_index+:$bits(byte)]}};
        bits_index += $bits(byte);
    end

    if (value.size() > 0) begin
        const int last_index = value.size() - 1;
        value[last_index] = mask_byte(value[last_index], size);
    end

    m_unpacked += size;
endfunction

function void packer::unpack_ints(ref int value[], input int size = -1);
    int bits_index = m_unpacked;

    size = get_unpack_size(size);
    value = new [align_bits_to_ints(size)];

    foreach (value[i]) begin
        value[i] = {>>{m_bits[bits_index+:$bits(int)]}};
        bits_index += $bits(int);
    end

    if (value.size() > 0) begin
        const int last_index = value.size() - 1;
        value[last_index] = mask_int(value[last_index], size);
    end

    m_unpacked += size;
endfunction

function string packer::unpack_string();
    int size = get_unpack_size();
    string value = "";

    while (size > 0) begin
        byte char = {>>{m_bits[m_unpacked+:$bits(byte)]}};

        if (size >= $bits(byte)) begin
            m_unpacked += $bits(byte);
        end
        else begin
            m_unpacked += size;
            char = mask_byte(char, size);

            `uvm_error("UVM/BASE/PACKER/BAD_SIZE",
                $sformatf(".unpack_string() method detect last char width as '%0d' bits not required '%0d' bits",
                    size,
                    $bits(byte)
                )
            )
        end

        size -= $bits(byte);

        if (char != 0) begin
            value = {value, char};
        end
        else begin
            size = 0;
        end
    end

    return value;
endfunction

function time packer::unpack_time();
    return time'(unpack_field_int($bits(time)));
endfunction

function real packer::unpack_real();
    return $bitstoreal(unpack_field_int(64));
endfunction

function void packer::unpack_object(uvm_object value);
    if (value != null) begin
        uvm_field_op field_op;

        push_active_object(value);

        field_op = uvm_field_op::m_get_available_op() ;
        field_op.set(UVM_UNPACK, this, value);
        value.do_execute_op(field_op);

        if (field_op.user_hook_enabled()) begin
            value.do_unpack(this);
        end

        field_op.m_recycle();

        void'(pop_active_object());
    end
    else begin
        `uvm_error("UVM/BASE/PACKER/NULL", ".unpack_object() method called with an UVM object as a null")
    end
endfunction

function int packer::get_unpack_size(int size = -1);
    const int unpack = m_packed - m_unpacked;

    if (size < 0) begin
        size = unpack;
    end
    else if (size > unpack) begin
        size = unpack;

        `uvm_error("UVM/BASE/PACKER/BAD_SIZE",
            $sformatf("unpack method called with size '%0d', which exceeds available packed bits of '%0d'",
                size,
                unpack
            )
        )
    end

    return size;
endfunction

function void packer::allocate_memory(int size);
    // Forward ahead allocation
    size += FORWARD_ALLOCATION_SIZE;

    if (size > m_bits.size()) begin
        m_bits = new [size + DEFAULT_MEMORY_SIZE];
    end
endfunction

function void packer::reallocate_memory(int size);
    // Forward ahead allocation
    size += (m_packed + FORWARD_ALLOCATION_SIZE);

    if (size > m_bits.size()) begin
        m_bits = new [size + DEFAULT_MEMORY_SIZE] (m_bits);
    end
endfunction

function int packer::align_bits_to_bytes(int bits);
    return (bits + $bits(byte) - 1) / $bits(byte);
endfunction

function int packer::align_bits_to_ints(int bits);
    return (bits + $bits(int) - 1) / $bits(int);
endfunction

function int packer::align_bits_to_longints(int bits);
    return (bits + $bits(longint) - 1) / $bits(longint);
endfunction

function byte packer::mask_byte(byte value, int size);
    int remainder = (size % $bits(byte));

    if (remainder != 0) begin
        value &= ({$bits(byte){1'b1}} << ($bits(byte) - remainder));
    end

    return value;
endfunction

function int packer::mask_int(int value, int size);
    int remainder = (size % $bits(int));

    if (remainder != 0) begin
        value &= ({$bits(int){1'b1}} << ($bits(int) - remainder));
    end

    return value;
endfunction

function longint packer::mask_longint(longint value, int size);
    int remainder = (size % $bits(longint));

    if (remainder != 0) begin
        value &= ({$bits(longint){1'b1}} << ($bits(longint) - remainder));
    end

    return value;
endfunction

`endif /* UVM_EXTENSIONS_PACKER_SVH */
