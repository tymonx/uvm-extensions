/* Copyright 2020 Tymoteusz Blazejczyk
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

`ifndef UVM_EXTENSIONS_CORESERVICE_SVH
`define UVM_EXTENSIONS_CORESERVICE_SVH

/* Class: coreservice
 *
 * UVM coreservice extension
 *
 * Reason:
 * https://forums.accellera.org/topic/6572-annoying-and-useless-packer-implementation-in-the-uvm-2017-v10-library-code-release
 *
 * Adding the .clone() call for .get_default_*() methods
 *
 * Methods are not thread safe and they have side effects because of using a singleton pattern
 */
class coreservice extends uvm_default_coreservice_t;
    /* Public */
    extern function new();

    extern static function uvm_coreservice_t create();

    extern function void copy(uvm_coreservice_t other);

    extern virtual function uvm_packer get_default_packer();
endclass

function coreservice::new();
    super.new();

    copy(uvm_coreservice_t::get());
endfunction

function uvm_coreservice_t coreservice::create();
    coreservice inst = new();

    return inst;
endfunction

function void coreservice::copy(uvm_coreservice_t other);
    set_factory(other.get_factory());
    set_uvm_seeding(other.get_uvm_seeding());
    set_resource_pool(other.get_resource_pool());
    set_report_server(other.get_report_server());
    set_default_copier(other.get_default_copier());
    set_default_packer(other.get_default_packer());
    set_default_printer(other.get_default_printer());
    set_default_comparer(other.get_default_comparer());
    set_component_visitor(other.get_component_visitor());
    set_default_tr_database(other.get_default_tr_database());
    set_phase_max_ready_to_end(other.get_phase_max_ready_to_end());
    set_resource_pool_default_precedence(other.get_resource_pool_default_precedence());
endfunction

function uvm_packer coreservice::get_default_packer();
    uvm_object object = super.get_default_packer().clone();
    uvm_packer packer = null;

    void'($cast(packer, object));

    return packer;
endfunction

`endif /* UVM_EXTENSIONS_CORESERVICE_SVH */
